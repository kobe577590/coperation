package com.tianpengtech.exception;

public class ValidateException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValidateException(String msg){
		super(msg);
	}
}
