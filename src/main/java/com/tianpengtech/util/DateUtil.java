package com.tianpengtech.util;

public class DateUtil {

	public static String frindlyTime(long seconds){
		
		long ONE_SECOND=1;
		long ONE_MINUTE=ONE_SECOND*60;
		long ONE_HOUR=ONE_MINUTE*60;
		long ONE_DAY=ONE_HOUR*24;
		long ONE_MOUTH=ONE_DAY*30;
		long ONE_YEAR=ONE_DAY*365;
		StringBuffer sb = new StringBuffer();
		
		if(seconds>=ONE_SECOND && seconds<ONE_MINUTE){
			int minutes = (int) (seconds/ONE_MINUTE);
			sb.append(minutes);
			sb.append("分钟前");
		}
		if(seconds>=ONE_HOUR && seconds<ONE_DAY){
			int hours = (int) (seconds/(ONE_HOUR));
			sb.append(hours);
			sb.append("小时前");
		}
		if(seconds>=ONE_DAY && seconds<ONE_MOUTH){
			int days = (int) (seconds/(ONE_DAY));
			sb.append(days);
			sb.append("天前");
		}
		
		if(seconds>=ONE_MOUTH && seconds<ONE_YEAR){
			int days = (int) (seconds/(ONE_MOUTH));
			sb.append(days);
			sb.append("个月前");
		}
		
		if(seconds>=ONE_YEAR){
			int days = (int) (seconds/(ONE_YEAR));
			sb.append(days);
			sb.append("年前");
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		long second = 3600*24*365*5;
		System.out.println(DateUtil.frindlyTime(second));
	}
}
